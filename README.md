## **t-io: 让网络编程更轻松和有趣**

[![image](https://gitee.com/tywo45/t-io/raw/master/docs/logo/preview.png)](http://www.t-io.org/#/doc)




## **t-io诞生的意义**
- 旧时王谢堂前燕，飞入寻常百姓家----当年那些王谢贵族们才拥有的"百万级即时通讯"应用，将因为t-io的诞生，纷纷飞入普通人家的屋檐下。

### **[t-io文档](http://t-io.org/#/doc "t-io文档")**





