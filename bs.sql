/*
Navicat MariaDB Data Transfer

Source Server         : tio-site
Source Server Version : 100027
Source Host           : localhost:3306
Source Database       : tiosite_main

Target Server Type    : MariaDB
Target Server Version : 100027
File Encoding         : 65001

Date: 2018-08-17 17:53:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bs
-- ----------------------------
DROP TABLE IF EXISTS `bs`;
CREATE TABLE `bs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `personid` int(11) NOT NULL COMMENT '提供者，对应person表的id',
  `casename` varchar(64) NOT NULL,
  `caseintro` varchar(255) NOT NULL COMMENT '简介',
  `caseurl` varchar(255) DEFAULT NULL COMMENT '连接地址',
  `caseimg` varchar(255) DEFAULT NULL COMMENT '展示图片地址',
  `provideddate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '案例提供日期，不一定准确',
  `remark` varchar(255) DEFAULT NULL,
  `orderby` int(11) NOT NULL DEFAULT '100000' COMMENT '排序号，值越小越在前面',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COMMENT='tio案例';

-- ----------------------------
-- Records of bs
-- ----------------------------
INSERT INTO `bs` VALUES ('1', '23', '氦氪云', 'tio为氦氪云提供网络接入层服务，强强联合，产品质量得到双重保障', '/api/ad/11.php', '/bs/res/hk/1.png', '2018-04-15 14:13:59', null, '2');
INSERT INTO `bs` VALUES ('30', '18', '优客服', '目标远大的在智能客服平台、呼叫中心', '/api/ad/1.php', '/bs/res/uke/1.png', '2018-04-18 14:01:03', null, '1000');
INSERT INTO `bs` VALUES ('32', '24', '长飞光纤光缆有限公司', 'tio为长飞提供DTS8000接入协议的实现、商用级网络接入架构与框架', '/api/ad/13.php', '/bs/res/cf/1.png', '2018-08-17 17:37:55', null, '1');
